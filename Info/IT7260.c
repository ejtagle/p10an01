
#define MAX_FINGER_NUM  3
#define DEVICE_ADDRESS 0x8C

#define COMMAND_BUFFER_INDEX 0x20
#define QUERY_BUFFER_INDEX 0x80
#define COMMAND_RESPONSE_BUFFER_INDEX 0xA0
#define POINT_BUFFER_INDEX 0xE0

#define QUERY_SUCCESS 0x00
#define QUERY_BUSY 0x01
#define QUERY_ERROR 0x02
#define QUERY_POINT 0x80

#define IIC_SDA sbIICSDA
#define IIC_SCLK sbIICSCLK
#define LOW 0
#define HIGH  1
#define ACK LOW
#define NAK HIGH
#define DELAY_TIME 6

// SDA signal of I2C 

// SCLK signal of I2C 

// Psudo functions 

VOID I2CSDASetInput(VOID);

VOID I2CSDASetOutput(VOID);

VOID I2CSCLKSetInput(VOID);

VOID I2CSCLKSetOutput(VOID);

VOID Delay(unsigned int unNs);

// Set I2C SDA signal as input 

// Set I2C SDA signal as output 

// Set I2C SCLK signal as input 

// Set I2C SCLK signal as output 

// Delay for unNs nano seconds 42 

// Start bit of I2C waveform 

VOID I2CStart(VOID)
{
	I2CSDASetOutput();
	I2CSCLKSetOutput();
	IIC_SDA = HIGH;
	Delay(DELAY_TIME);
	IIC_SCLK = HIGH;
	Delay(DELAY_TIME);
	IIC_SDA = LOW;
	Delay(DELAY_TIME);
	IIC_SCLK = HIGH;
	Delay(DELAY_TIME);
	IIC_SDA = LOW;
	Delay(DELAY_TIME);
	IIC_SCLK = LOW;
	Delay(DELAY_TIME);
}

// Stop bit of I2C waveform 

VOID I2CStop(VOID)
{
	I2CSDASetOutput();
	I2CSCLKSetOutput();
	IIC_SDA = LOW;
	Delay(DELAY_TIME);
	IIC_SCLK = HIGH;
	Delay(DELAY_TIME);
	IIC_SDA = HIGH;
	Delay(DELAY_TIME);
	IIC_SCLK = HIGH;

	Delay(DELAY_TIME);
}

// Send one byte from host to client 
BOOL I2CSendByte(BYTE ucData)
{
	BOOL bRet;
	int i;
	BYTE ucMask;
	I2CSDASetOutput();
	I2CSCLKSetOutput();
	for (i = 0, ucMask = 0x80; i < 8; i++, ucMask >>= 1) {
		IIC_SDA = ucData & ucMask;
		Delay(DELAY_TIME);
		IIC_SCLK = HIGH;
		Delay(DELAY_TIME);
		IIC_SCLK = LOW;
		Delay(DELAY_TIME);
	}
	I2CSDASetInput();
	I2CSCLKSetOutput();
	bRet = IIC_SDA;
	IIC_SCLK = HIGH;
	Delay(DELAY_TIME);
	IIC_SCLK = LOW;
	Delay(DELAY_TIME);
	return bRet;
}

// Receive one byte form client to host 
BYTE I2CReciveByte(BOOL bAck)
{
	BYTE ucRet;
	int i;
	I2CSDASetInput();
	I2CSCLKSetOutput();
	ucRet = 0;
	for (i = 7; i >= 0; i--) {
		ucRet |= (IIC_SDA << i);
		IIC_SCLK = HIGH;
		Delay(DELAY_TIME);
		IIC_SCLK = LOW;
		Delay(DELAY_TIME);
	}
	I2CSDASetOutput();
	I2CSCLKSetOutput();
	IIC_SDA = bAck;
	Delay(DELAY_TIME);
	IIC_SCLK = HIGH;
	Delay(DELAY_TIME);
	IIC_SCLK = LOW;
	Delay(DELAY_TIME);
	return ucRet;
}

// I2C send data fuction 
BOOL I2CSend(BYTE ucDeviceAddr, BYTE ucBufferIndex, BYTE * pucData, unsigned int unDataLength)
{
	unsigned int i;

	BOOL bRet = true;

	I2CStart();
	if (I2CSendByte(ucDeviceAddr & 0xFE) == ACK) {
		if (I2CSendByte(ucBufferIndex) == ACK) {
			for (i = 0; i < unDataLength; i++) {
				if (I2CSendByte(pucData[i]) == NAK) {
					bRet = false;
					break;
				}
			}
		} else {
			bRet = false;
		}
	} else {
		bRet = false;
	}
	I2CStop();
	return bRet;
}

// I2C receive data function 
BOOL I2CReceive(BYTE ucDeviceAddr, BYTE ucBufferIndex, BYTE * pucData, unsigned int unDataLength)
{
	unsigned int i;

	BOOL bRet = true;
	I2CStart();

	if (I2CSendByte(ucDeviceAddr & 0xFE) == ACK) {
		if (I2CSendByte(ucBufferIndex) == ACK) {
			I2CStart();
			if (I2CSendByte(ucDeviceAddr | 0x01) == ACK) {
				for (i = 0; i < unDataLength - 1; i++) {
					pucData[i] = I2CReciveByte(ACK);
				}
				pucData[unDataLength - 1] = I2CReciveByte(NAK);
			} else {
				bRet = false;
			}
		} else {
			bRet = false;
		}
	} else {
		bRet = false;
	}
	I2CStop();
	return bRet;
}

BOOL ReadQueryBuffer(BYTE * pucData)
{
	return I2CReceive(DEVICE_ADDRESS, QUERY_BUFFER_INDEX, pucData, 1);
}

BOOL ReadCommandResponseBuffer(BYTE * pucData, unsigned int unDataLength)
{
	return I2CReceive(DEVICE_ADDRESS, COMMAND_RESPONSE_BUFFER_INDEX, pucData, unDataLength);
}

BOOL ReadPointBuffer(BYTE * pucData)
{
	return I2CReceive(DEVICE_ADDRESS, POINT_BUFFER_INDEX, pucData, 14);
}

BOOL WriteCommandBuffer(BYTE * pucData, unsigned int unDataLength)
{
	return I2CSend(DEVICE_ADDRESS, COMMAND_BUFFER_INDEX, pucData, unDataLength);
}

// ================================================================================ 
// Function Name --- IdentifyCapSensor 
// Description --- Identify Capacitive Sensor information 
// Input --- NULL 
// Output --- return true if the command execute successfully, otherwuse return false. 
// ================================================================================ 
BOOL IdentifyCapSensor()
{
	BYTE ucWriteLength, ucReadLength;
	BYTE pucData[128];
	BYTE ucQuery;

	ucWriteLength = 1;
	ucReadLength = 0x0A;
	pucData[0] = 0x00;

	// Query 
	do {
		if (!ReadQueryBuffer(&ucQuery)) {
			ucQuery = QUERY_BUSY;
		}
	} while (ucQuery & QUERY_BUSY);

	// Write Command 
	if (!WriteCommandBuffer(pucData, ucWriteLength)) {
		return false;
	}
	// Query 

#ifdef INTERRUPT_MODE
	Wait4INT();

#endif
	do {
		if (!ReadQueryBuffer(&ucQuery)) {
			ucQuery = QUERY_BUSY;
		}
	} while (ucQuery & QUERY_BUSY);

	// Read Command Response 
	if (!ReadCommandResponseBuffer(pucData, ucReadLength)) {
		return false;
	}
	if (pucData[1] != 'I' || pucData[2] != 'T' || pucData[3] != 'E' || pucData[4] != '7' || pucData[5] != '2'
	    || pucData[6] != '6' || pucData[7] != '0') {

		// firmware signature is not match 
		return false;
	}
	return true;
}

// ================================================================================ 
// Function Name --- GetFirmwareInformation 
// Description --- Get firmware information 
// Input --- NULL 
// Output --- return true if the command execute successfully, otherwuse return false. 
// ================================================================================ 
BOOL GetFirmwareInformation()
{
	BYTE ucWriteLength, ucReadLength;
	BYTE pucData[128];
	BYTE ucQuery;
	ucWriteLength = 2;
	ucReadLength = 0x09;
	pucData[0] = 0x01;
	pucData[1] = 0x00;

	// Query 
	do {
		if (!ReadQueryBuffer(&ucQuery))
			50 {
			ucQuery = QUERY_BUSY;
			}
	} while (ucQuery & QUERY_BUSY);

	// Write Command 
	if (!WriteCommandBuffer(pucData, ucWriteLength)) {
		return false;
	}
	// Query 
#ifdef INTERRUPT_MODE
	Wait4INT();
#endif
/* 
do { 
	if(!ReadQueryBuffer(&ucQuery)) 
	{ 
		ucQuery = QUERY_BUSY; 
	} 
} while (ucQuery & QUERY_BUSY) ;
*/

// Read Command Response 
	if (!ReadCommandResponseBuffer(pucData, ucReadLength)) {
		return false;
	}
	if (pucData[5] == 0 && pucData[6] == 0 && pucData[7] == 0 && pucData[8] == 0) {
		// There is no flash code 
		return false;
	}
	return true;
}

// ================================================================================ 
// Function Name --- Get2DResolutions 
// Description --- Get the resolution of X and Y axes 
// Input --
// 
// 
// pwXResolution � the X resolution pwYResolution � the Y resolution pucStep � the step 
// Output --- return true if the command execute successfully, otherwuse return false. 
// ================================================================================ 
BOOL Get2DResolutions(WORD * pwXResolution, WORD * pwYResolution, BYTE * pucStep)
{
	BYTE ucWriteLength, ucReadLength;
	BYTE pucData[128];
	BYTE ucQuery;
	ucWriteLength = 3;
	ucReadLength = 0x07;
	pucData[0] = 0x01;
	pucData[1] = 0x02;
	pucData[2] = 0x00;

// Query 
	do {
		if (!ReadQueryBuffer(&ucQuery)) {
			ucQuery = QUERY_BUSY;
		}
	} while (ucQuery & QUERY_BUSY);

// Write Command 
	if (!WriteCommandBuffer(pucData, ucWriteLength)) {
		return false;
	}
// Query 
#ifdef INTERRUPT_MODE
	Wait4INT();

#endif
/* 
do { 
if(!ReadQueryBuffer(&ucQuery)) 
{
 ucQuery = QUERY_BUSY; 
 
}
} while (ucQuery & QUERY_BUSY) ; */

// Read Command Response 
	if (!ReadCommandResponseBuffer(pucData, ucReadLength)) {
		return false;
	}
	if (pwXResolution != NULL) {
		*pwXResolution = pucData[2] + (pucData[3] << 8);
	}
	if (pwYResolution != NULL) {
		*pwYResolution = pucData[4] + (pucData[5] << 8);
	}
	if (pucStep != NULL) {
		*pucStep = pucData[6];
	}
	return true;
}

// ================================================================================ 53 

// Function Name --- SetInterruptNotification 
// Description --- Set It7260 interrupt mode 
// Input --
// 
// ucStatus� the interrupt status ucType� the interrupt type 
// Output --- return true if the command execute successfully, otherwuse return false. 
// ================================================================================ 
BOOL SetInterruptNotification(BYTE ucStatus, BYTE ucType)
{
	BYTE ucWriteLength, ucReadLength;
	BYTE pucData[128];
	BYTE ucQuery;
	ucWriteLength = 2;
	ucReadLength = 2;
	pucData[0] = 0x02;
	pucData[1] = 0x04;
	pucData[2] = ucStatus;
	pucData[3] = ucType;

// Query 
	do {
		if (!ReadQueryBuffer(&ucQuery)) {
			ucQuery = QUERY_BUSY;
		}
	} while (ucQuery & QUERY_BUSY);

// Write Command 
	if (!WriteCommandBuffer(pucData, ucWriteLength)) {
		return false;
	}
// Query 
#ifdef INTERRUPT_MODE 54
	Wait4INT();

#endif
/*
do { if(!ReadQueryBuffer(&ucQuery)) { ucQuery = QUERY_BUSY; 
}
}

while (ucQuery & QUERY_BUSY) ; */

// Read Command Response 
	if (!ReadCommandResponseBuffer(pucData, ucReadLength)) {
		return false;
	}
	if (*(WORD *) pucData != 0) {
		return false;
	}
	return true;
}

// ******************************************************************************************* 
// Function Name: CaptouchHWInitial 
// Description: 
// This function is mainly used to initialize cap-touch controller to active state. 
// Parameters: NULL 
// Return value: 
// return zero if success, otherwise return non zero value 
// ******************************************************************************************* 
int CaptouchHWInitial()
{
	if (!IdentifyCapSensor()) {
		return -1;
	}
	if (!GetFirmwareInformation()) {
		return -1;
	}
	if (!Get2DResolutions(pwXResolution, pwYResolution, pucStep)) {
		return -1;
	}
	if (!SetInterruptNotification(ucStatus, ucType)) {
		return -1;
	}
	return 0;
}

// ******************************************************************************************* 
// Function Name: CaptouchInterruptHandle 
// Description: 
// 
// 
// This function is mainly used to handle the interrupt. When host have received an interrupt, software on host will call this function to detect interrupt type, and call other functions to get the sample value, and other information. 
// Parameters: NULL 
// Return value: 
// return zero if success, otherwise return non zero value 
// ******************************************************************************************* 
int CaptouchInterruptHandle()
{
	BYTE ucQueryResponse;
	DWORD dwTouchEvent;
	while (!ReadQueryBuffer(&ucQueryResponse)) {
	}
// Touch Event 
	if (ucQueryResponse & QUERY_POINT) {
		if (!ReadPointBuffer(gpucPointBuffer)) {
			return -1;
		}
		switch (gpucPointBuffer[0] & 0xF0) {
		case 0x00:
			dwTouchEvent = CaptouchGetSampleValue(gpdwSampleX, gpdwSampleY, gpdwPressure);
			if (dwTouchEvent == 0) {
				SynchroSystemEvent(SYSTEM_TOUCH_EVENT_FINGER_RELEASE);
			} else if (dwTouchEvent <= CTP_MAX_FINGER_NUMBER) {
				SynchroSystemEvent(SYSTEM_TOUCH_EVENT_FINGER_ASSERT);
			} else {
				SynchroSystemEvent(SYSTEM_TOUCH_EVENT_PALM_DETECT);
			}
			break;
		default:
			break;
		}
	}
	return 0;
}

// ******************************************************************************************* 

// Function Name: CaptouchPollingHandle 
// Description: 
// This function is mainly used to handle the system event under polling mode. 
// Parameters: NULL 
// Return value: 
// return TRUE if success, otherwise return FALSE 
// ******************************************************************************************* 
BOOL CaptouchPollingHandle()
{
	BYTE ucQueryResponse;
	DWORD dwTouchEvent;
	if (!ReadQueryBuffer(&ucQueryResponse)) {
		return FALSE;
	}
// Touch Event 
	if (ucQueryResponse & QUERY_POINT) {
		if (!ReadPointBuffer(gpucPointBuffer)) {
			return FALSE;
		}
		switch (gpucPointBuffer[0] & 0xF0) {
		case 0x00:
			dwTouchEvent = CaptouchGetSampleValue(gpdwSampleX, gpdwSampleY, gpdwPressure);
			if (dwTouchEvent == 0) {
				SynchroSystemEvent(SYSTEM_TOUCH_EVENT_FINGER_RELEASE);
			} else if (dwTouchEvent <= CTP_MAX_FINGER_NUMBER) {
				SynchroSystemEvent(SYSTEM_TOUCH_EVENT_FINGER_ASSERT);
			} else {
				SynchroSystemEvent(SYSTEM_TOUCH_EVENT_PALM_DETECT);
			}
			break;
		default:
			break;
		}
	}
	return TRUE;
}

// ******************************************************************************************* 
// Function Name: CaptouchGetSampleValue 
// Description: 
// This function is mainly used to get sample values which shall contain x, y position and pressure. 
// Parameters: 
// 
// 
// pdwSampleX -- the pointer of returned X coordinates pdwSampleY -- the pointer of returned Y coordinates pdwPressure -- the pointer of returned pressures 
// Return value: 
// The return value is the number of sample points. Return 0 if failing to get sample. The maximum 
// return value is 10 in normal case. If the CTP controller does not support pressure measurement, 
// PRESSURE_INVALID. the return value is the sample values OR with 
// ******************************************************************************************* 
int CaptouchGetSampleValue(DWORD * pdwSampleX, DWORD * pdwSampleY, DWORD * pdwPressure)
{
	int nRet;
	int i;
	if (gpucPointBuffer[1] & 0x01) {
		return MAX_FINGER_NUM + 1;
	}
	nRet = 0;
	for (i = 0; i < MAX_FINGER_NUM; i++) {
		if (gpucPointBuffer[0] & (1 << i)) {
			nRet++;
			pdwSampleX[i] =
			    ((DWORD) (gpucPointBuffer[i * 4 + 3] & 0x0F) << 8) + (DWORD) gpucPointBuffer[i * 4 + 2];
			;
			pdwSampleY[i] =
			    ((DWORD) (gpucPointBuffer[i * 4 + 3] & 0xF0) << 4) + (DWORD) gpucPointBuffer[i * 4 + 4];
			pdwPressure[i] = (DWORD) (gpucPointBuffer[i * 4 + 5] & 0x0F);
		} else {
			pdwSampleX[i] = 0;
			pdwSampleY[i] = 0;
			pdwPressure[i] = 0;
		}
	}
	return nRet;
}

// ******************************************************************************************* 
// Function Name: CaptouchMode 
// Description: 
// This function is mainly used to initialize cap-touch controller to active state. 
// Parameters: 
// dwMode -- the power state to be entered 
// Return value: 
// return zero if success, otherwise return non zero value 
// ******************************************************************************************* 
int CaptouchMode(DWORD dwMode)
{
	BYTE ucQueryResponse;
	BYTE pucCommandBuffer[128];

	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	pucCommandBuffer[0] = 0x04;
	pucCommandBuffer[1] = 0x00;
	switch (dwMode) {
	case ACTIVE_MODE:
		pucCommandBuffer[2] = 0x00;
		break;
	case SLEEP_MODE:
		pucCommandBuffer[2] = 0x01;
		break;
	case POWEROFF_MODE:
		pucCommandBuffer[2] = 0x02;
		break;
	default:
		return -1;
	}
	if (!WriteCommandBuffer(3, pucCommandBuffer)) {
		return -1;
	}
	return 0;
}

// ******************************************************************************************* 
// Function Name: CaptouchSetPara 
// Description: 
// This function is mainly used to set various parameters of CTP controller including threshold 
// setting. 
// Parameters: 
// dwMode -- the power state to be entered 61 

// value -
// Return value: 
// return zero if success, otherwise return non zero value 
// ******************************************************************************************* 
int CaptouchSetPara(DWORD dwMode, DWORD value)
{
	BYTE ucQueryResponse;
	BYTE pucCommandBuffer[128];
	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	switch (dwMode) {
	case THRESHOLD_PARA:
		if (value > 100) {
			return -1;
		}
		pucCommandBuffer[0] = 0x15;
		pucCommandBuffer[1] = 0x00;
		pucCommandBuffer[2] = (BYTE) (value & 0x00FF);
		pucCommandBuffer[3] = (BYTE) ((value & 0xFF00) >> 8);
		if (!WriteCommandBuffer(4, pucCommandBuffer)) {
			return -1;
		}
		break;
	default:
		return -1;
	}
#ifdef INTERRUPT_MODE
	Wait4INT();

#endif
	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	if (!ReadCommandResponseBuffer(2, pucCommandBuffer)) {
		return -1;
	}
	if (pucCommandBuffer[0] == 0 && pucCommandBuffer[1] == 0) {
		return 0;
	}
	return -1;
}

// ******************************************************************************************* 
// Function Name: CaptouchReset 
// Description: 
// This function is mainly used to reset cap-touch controller by sending reset command. 
// Parameters: NULL 
// Return value: 
// return TRUE if success, otherwise return FALSE 
// ******************************************************************************************* 
int CaptouchReset()
{
	BYTE ucQueryResponse;
	BYTE pucCommandBuffer[128];
	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	pucCommandBuffer[0] = 0x6F;
	if (!WriteCommandBuffer(1, pucCommandBuffer)) {
		return -1;
	}
	Delay(200);

#ifdef INTERRUPT_MODE
	Wait4INT();

#endif
	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	if (!ReadCommandResponseBuffer(2, pucCommandBuffer)) {
		return -1;
	}
	if (pucCommandBuffer[0] == 0 && pucCommandBuffer[1] == 0) {
		return 0;
	}
	return -1;
}

// ******************************************************************************************* 
// Function Name: CaptouchGetversion 
// Description: 
// This function is mainly used to get firmware version. 
// Parameters: NULL 64 

// Return value: 
// return 0 if failing to get CTP version, otherwise return the CTP version. 
// ******************************************************************************************* 
int CaptouchGetversion()
{
	BYTE ucQueryResponse;
	BYTE pucCommandBuffer[128];

// Get firmware information 
	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	pucCommandBuffer[0] = 0x01;
	pucCommandBuffer[1] = 0x00;
	if (!WriteCommandBuffer(2, pucCommandBuffer)) {
		return 0;
	}
#ifdef INTERRUPT_MODE
	Wait4INT();

#endif
	do {
		ReadQueryBuffer(&ucQueryResponse);
	} while (ucQueryResponse & QUERY_BUSY);
	if (!ReadCommandResponseBuffer(9, pucCommandBuffer)) {
		return 0;
	}
	return (pucCommandBuffer[5] << 24) + (pucCommandBuffer[6] << 16) + (pucCommandBuffer[7] << 8) +
	    pucCommandBuffer[8];
}

#define BUFFER_INDEX_COMMAND 0x20
#define BUFFER_INDEX_QUERY 0x80
#define BUFFER_INDEX_COMMAND_RESPONSE 0xA0
#define MAX_BUFFER_SIZE 256
#define QUERY_COMMAND_READY 0x00
#define QUERY_COMMAND_BUSY 0x01
#define QUERY_COMMAND_ERROR 0x02
#define COMMAND_RESPONSE_SUCCESS 0x0000
#define SIGNATURE_LENGTH 16

// ***************************************************************************** 
// Function name: fnFirmwareDownload 
// 
// Description: 
// 
// 
// Arguments: 
// unFirmwareLength -- the file length of firmware file. 
// 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
//pFirmware -- the firmware binary file. unConfigLength -- the file length of configuration file. pConfig -- the configuration binary file. This function is used to download firmware and configuration files. 66 

BOOL fnFirmwareDownload(UINT unFirmwareLength, VOID * pFirmware, UINT unConfigLength, VOID * pConfig)
{
	if ((unFirmwareLength != 0 && pFirmware == NULL) || (unConfigLength != 0 && pConfig == NULL)) {
		// Input invalidate 
		return FALSE;
	} else if (unFirmwareLength == 0 && unConfigLength == 0) {
		// Need not to download anything 
		return TRUE;
	}
	if (!fnEnterFirmwareUpgradeMode()) {
		return FALSE;
	}
	if (unFirmwareLength != 0 && pFirmware == NULL) {
		// Download firmware 
		if (!fnDownloadFirmware(unFirmwareLength, pFirmware)) {
			return FALSE;
		}
	}
	if (unConfigLength != 0 && pConfig == NULL) {
		// Download configuration 
		if (!fnDownloadConfig(unConfigLength, pConfig)) {
			return FALSE;
		}
	}
	if (!fnExitFirmwareUpgradeMode())
		67 {
		return FALSE;
		}
	if (!fnFirmwareReinitialize()) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnDownloadFirmware 
// 
// Description: 
// 
// 
// Arguments: 
// unFirmwareLength -- the file length of firmware file. 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnDownloadFirmware(UINT unFirmwareLength, VOID * pFirmware)
{
	if (!fnVerifyFirmware(unFirmwareLength, pFirmware)) {
		return FALSE;
	}
	pFirmware-- the firmware binary file.This function is used to download firmware file.if (!fnSetStartOffset(0)) {
		return FALSE;
	}
	if (!fnAdvanceWriteFlash(unFirmwareLength, pFirmware)) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnDownloadFirmware 
// 
// Description: 
// 
// 
// Arguments: 
// unFirmwareLength -- the file length of firmware file. 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnVerifyFirmware(UINT unFirmwareLength, VOID * pFirmware)
{
	WORD wFlashSize;
	WORD wSize;
	BYTE pucBuffer[SIGNATURE_LENGTH];
	// pFirmware-- the firmware binary file.This function is used to verify firmware file.
// Check signature 
	if (*((BYTE *) pFirmware + 0) != 'I' || *((BYTE *) pFirmware + 1) != 'T'
	    || *((BYTE *) pFirmware + 2) != '7' || *((BYTE *) pFirmware + 3) != '2'
	    || *((BYTE *) pFirmware + 4) != '6' || *((BYTE *) pFirmware + 5) != '0'
	    || *((BYTE *) pFirmware + 6) != 'F' || 69 * ((BYTE *) pFirmware + 7) != 'W') {
		return FALSE;
	}
// Check overlap with config file 
	if (!fnGetFlashSize(&wFlashSize)) {
		return FALSE;
	}
	if (!fnSetStartOffset(wFlashSize - SIGNATURE_LENGTH)) {
		return FALSE;
	}
	if (!fnReadFlash(SIGNATURE_LENGTH, pucBuffer)) {
		return FALSE;
	}
	if (pucBuffer[0] == 'C' && pucBuffer[1] == 'O' && pucBuffer[2] == 'N' && pucBuffer[3] == 'F'
	    && pucBuffer[4] == 'I' && pucBuffer[5] == 'G') {
		memcpy(&wSize, pucBuffer + 6, sizeof(WORD));
		if (wSize + (WORD) unFirmwareLength >= wFlashSize) {
			return FALSE;
		}
	}
	return TRUE;
}

70
// ***************************************************************************** 
// Function name: fnDownloadConfig 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
    BOOL fnDownloadConfig(UINT unConfigLength, VOID * pConfig)
{
	WORD wFlashSize;
	//unConfigLength-- the file length of configuration file.pConfig-- the configuration binary file.
	//This function is used to download configuration file.
	if (!fnVerifyConfig(unConfigLength, pConfig)) {
		return FALSE;
	}
	if (!fnGetFlashSize(&wFlashSize)) {
		return FALSE;
	}
	if (!fnSetStartOffset(wFlashSize - (WORD) unConfigLength)) {
		return FALSE;
	}
	if (!fnAdvanceWriteFlash(unConfigLength, pConfig)) {
		return FALSE;

	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnVerifyConfig 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnVerifyConfig(UINT unConfigLength, VOID * pConfig)
{
	WORD wFlashSize;
	WORD wSize;
	BYTE pucBuffer[SIGNATURE_LENGTH];
	//unConfigLength-- the file length of configuration file.pConfig-- the configuration binary file.
	//  This function is used to verify configuration file.
// Check signature 
	if (*((BYTE *) pConfig + 0) != 'C' || *((BYTE *) pConfig + 1) != 'O' || *((BYTE *) pConfig + 2) != 'N'
	    || *((BYTE *) pConfig + 3) != 'F' || *((BYTE *) pConfig + 4) != 'I' || *((BYTE *) pConfig + 5) != 'G') {
		return FALSE;
	}
// Check overlap with config file 72 
	if (!fnGetFlashSize(&wFlashSize)) {
		return FALSE;
	}
	if (!fnSetStartOffset(0)) {
		return FALSE;
	}
	if (!fnReadFlash(SIGNATURE_LENGTH, pucBuffer)) {
		return FALSE;
	}
	if (pucBuffer[0] == 'I' && pucBuffer[0] == 'T' && pucBuffer[0] == '7' && pucBuffer[0] == '2'
	    && pucBuffer[0] == '6' && pucBuffer[0] == '0' && pucBuffer[0] == 'F' && pucBuffer[0] == 'W') {
		memcpy(&wSize, pucBuffer + 12, sizeof(WORD));
		if (wSize + (WORD) unConfigLength >= wFlashSize) {
			return FALSE;
		}
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnEnterFirmwareUpgradeMode 
// 73 

// Description: 
// 
// 
// Arguments: 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnEnterFirmwareUpgradeMode()
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	This function is used to enter firmware upgrade mode.
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x60;
	pucBuffer[1] = 0x00;
	pucBuffer[2] = 'I';
	pucBuffer[3] = 'T';
	pucBuffer[4] = '7';
	pucBuffer[5] = '2';
	pucBuffer[6] = '6';
	pucBuffer[7] = '0';
	pucBuffer[8] = 0x55;
	pucBuffer[9] = 0xAA;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 10, pucBuffer)) {
		74 return FALSE;
	}
#ifdef SUPPORT_INTERRUPT_MODE
	fnWaitInterrupt();

#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnExitFirmwareUpgradeMode 
// 
// Description: 
// 
// 
// Arguments: 
// This function is used to exit firmware upgrade mode. 75 

// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnExitFirmwareUpgradeMode()
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x60;
	pucBuffer[1] = 0x80;
	pucBuffer[2] = 'I';
	pucBuffer[3] = 'T';
	pucBuffer[4] = '7';
	pucBuffer[5] = '2';
	pucBuffer[6] = '6';
	pucBuffer[7] = '0';
	pucBuffer[8] = 0xAA;
	pucBuffer[9] = 0x55;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 10, pucBuffer)) {
		return FALSE;
	}
#ifdef SUPPORT_INTERRUPT_MODE
	fnWaitInterrupt();

#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnGetFlashSize 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** pwFlashSize -- the flash size of IT7260/50. This function is used to get IT7260/50 flash size. 77 

BOOL fnGetFlashSize(WORD * pwFlashSize)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	if (pwFlashSize == NULL) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x01;
	pucBuffer[1] = 0x03;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 2, pucBuffer)) {
		return FALSE;
	}
#ifdef SUPPORT_INTERRUPT_MODE
	fnWaitInterrupt();

#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	*pwFlashSize = wCommandResponse;
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnSetStartOffset 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnSetStartOffset(WORD wOffset)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	// wOffset-- the start offset of read / write flash.This function is used to set start offset of read /
	// write flash.
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			79 ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x61;
	pucBuffer[1] = 0;
	memcpy(pucBuffer + 2, &wOffset, sizeof(WORD));
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 4, pucBuffer)) {
		return FALSE;
	}
#ifdef SUPPORT_INTERRUPT_MODE
	fnWaitInterrupt();

#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnFirmwareReinitialize 
// 
// Description: 
// 
// 
// Arguments: 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnFirmwareReinitialize()
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	This function is used to reset IT7260 / 50 firmware.
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x6F;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 1, pucBuffer)) {
		return FALSE;
	}
	81
#ifdef SUPPORT_INTERRUPT_MODE
	    fnWaitInterrupt();
#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnWriteFlash 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// 
// Return Value: unLength -- the length of send flash data (Max length 128). pData -- the send flash data. This function is used to write flash data. 82 

// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnWriteFlash(UINT unLength, VOID * pData)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	if (unLength > 128 || pData == NULL) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x62;
	pucBuffer[1] = (BYTE) unLength;
	memcpy(pucBuffer + 2, pData,
	       unLength * sizeof(BYTE)) if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 2 + unLength, pucBuffer)) {
		return FALSE;
	}
#ifdef SUPPORT_INTERRUPT_MODE
	fnWaitInterrupt();

#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnAdvanceWriteFlash 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnAdvanceWriteFlash(UINT unLength, VOID * pData)
{
	// unLength-- the length of send flash data.pData-- the send flash data.This function is used to write flash data.
	UINT unCurRoundLength;
	UINT unRemainderLength = unLength;
	while (unRemainderLength > 0) {
		if (unRemainderLength > 128) {
			unCurRoundLength = 128;
		} else {
			unCurRoundLength = unRemainderLength;
		}
		if (fnWriteFlash(unCurRoundLength, pData + (unLength - unRemainderLength))) {
			return FALSE;
		}
		unRemainderLength -= unCurRoundLength;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnReadFlash 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. unLength -- the length of receive flash data (Max length 128).. pData -- the receive flash data. This function is used to read flash data. 85 

// 
// ***************************************************************************** 
BOOL fnReadFlash(UINT unLength, VOID * pData)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	if (unLength > 128 || pData == NULL) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x63;
	pucBuffer[1] = (BYTE) unLength;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 2, pucBuffer)) {
		return FALSE;
	}
#ifdef SUPPORT_INTERRUPT_MODE
	fnWaitInterrupt();

#endif
// SUPPORT_INTERRUPT_MODE 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, unLength, pucBuffer)) {
		return FALSE;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnAdvanceReadFlash 
// 
// Description: 
// 
// 
// Arguments: 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnAdvanceReadFlash(UINT unLength, VOID * pData)
{
	UINT unCurRoundLength;
	UINT unRemainderLength = unLength;
	//unLength-- the length of receive flash data(Max length 128)..pData-- the receive flash data.
	//  This function is used to read flash data.
	while (unRemainderLength > 0) {
		if (unRemainderLength > 128) {
			unCurRoundLength = 128;

		} else {
			unCurRoundLength = unRemainderLength;
		}
		if (fnReadFlash(unCurRoundLength, pData + (unLength - unRemainderLength))) {
			return FALSE;
		}
		unRemainderLength -= unCurRoundLength;
	}
	return TRUE;
}

// ***************************************************************************** 
// Function name: fnWriteBuffer 
// 
// Description: 
// 
// 
// Arguments: 
// ucBuffeIndex -- the buffer index of IT7260/50 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
BOOL fnWriteBuffer(BYTE ucBuffeIndex, UINT unLength, VOID * pData)
{
	// Sudo code, not implement here 
}

unLength-- the file length of send data.pData-- the send data.This function is used to send data to IT7260 / 50. 88
// ***************************************************************************** 
// Function name: fnReadBuffer 
// 
// Description: 
// 
// 
// Arguments: 
// ucBuffeIndex -- the buffer index of IT7260/50 
// 
// 
// 
// Return Value: 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
    BOOL fnReadBuffer(BYTE ucBuffeIndex, UINT unLength, VOID * pData)
{
	// Sudo code, not implement here 
}

unLength-- the file length of receive data.pData-- the receive data.This function is used to receive data from IT7260 /
    50.
// ***************************************************************************** 
// Function name: fnWaitInterrupt 
// 
// Description: 
// 
// 
// Arguments: 
// 
// Return Value: 
// 
// ***************************************************************************** void fnWaitInterrupt() { 
// Sudo code, not implement here } This function is used to wait until interrupt notified.                                             /*************************************** Firmware Upgrade for MTK ****************************************/
#define BUFFER_INDEX_COMMAND
#define BUFFER_INDEX_QUERY
#define BUFFER_INDEX_COMMAND_RESPONSE 0xA0 0x20 0x80
#define MAX_BUFFER_SIZE 256
#define QUERY_COMMAND_READY
#define QUERY_COMMAND_BUSY
#define QUERY_COMMAND_ERROR 0x00 0x01 0x02
#define COMMAND_RESPONSE_SUCCESS
#define SIGNATURE_LENGTH 0x0000 16
#define FLASH_HIGH_ADDR
#define FLASH_LOW_ADDR 0xD0 0x00
// ***************************************************************************** 
// Function name: fnFirmwareDownload 
// 
// Description: 
// 
// 
// Arguments: 
// unFirmwareLength -- the file length of firmware file. 
// 
// 
// 
// 
// Return Value: pFirmware -- the firmware binary file. unConfigLength -- the file length of configuration file. pConfig -- the configuration binary file. This function is used to download firmware and configuration files. 94 
// Return true if the function execute succefully, otherwise return false. 
// 
// ***************************************************************************** 
    BOOL fnFirmwareDownload(UINT unFirmwareLength, VOID * pFirmware, UINT unConfigLength, VOID * pConfig)
{
	if ((unFirmwareLength != 0 && pFirmware == NULL) || (unConfigLength != 0 && pConfig == NULL)) {
		// Input invalidate 
		return FALSE;
	} else if (unFirmwareLength == 0 && unConfigLength == 0) {
		// Need not to download anything 
		return TRUE;
	}
	if (!fnEnterFirmwareUpgradeMode()) {
		return FALSE;
	}
	if (unFirmwareLength != 0 && pFirmware != NULL) {
		// Download firmware 
		if (!fnDownloadFirmware(unFirmwareLength, pFirmware)) {
			return FALSE;
		}
	}
	if (unConfigLength != 0 && pConfig != NULL) {
		// Download configuration 
		if (!fnDownloadConfig(unConfigLength, pConfig)) {
			return FALSE;
		}
	95}
	if (!fnExitFirmwareUpgradeMode()) {
		return FALSE;
	}
	if (!fnFirmwareReinitialize()) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnDownloadFirmware 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// unFirmwareLength -- the file length of firmware file. 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnDownloadFirmware(UINT unFirmwareLength, VOID * pFirmware)
{
	if (!fnVerifyFirmware(unFirmwareLength, pFirmware)) {
		return FALSE;
	}
	pFirmware-- the firmware binary file.This function is used to download firmware file.
	    if (!fnSetStartOffset(0X0000)) {
		return FALSE;
	}
	if (!fnAdvanceWriteFlash(unFirmwareLength, pFirmware)) {
		return FALSE;
	}
	if (!fnCompareResult(unFirmwareLength, 0x0000, pFirmware)) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnVerifyFirmware 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// unFirmwareLength -- the file length of firmware file. 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnVerifyFirmware(UINT unFirmwareLength, VOID * pFirmware)
{
	WORD wFlashSize;
	WORD wSize;
	BYTE pucBuffer[SIGNATURE_LENGTH];
	//pFirmware-- the firmware binary file.This function is used to verify firmware file.97
	// Check signature 
	if (*((BYTE *) pFirmware + 0) != 'I' || *((BYTE *) pFirmware + 1) != 'T'
	    || *((BYTE *) pFirmware + 2) != '7' || *((BYTE *) pFirmware + 3) != '2'
	    || *((BYTE *) pFirmware + 4) != '6' || *((BYTE *) pFirmware + 5) != '0'
	    || *((BYTE *) pFirmware + 6) != 'F' || *((BYTE *) pFirmware + 7) != 'W') {
		return FALSE;
	}
	// Check overlap with config file 
	if (!fnGetFlashSize(&wFlashSize)) {
		return FALSE;
	}
	if (!fnAdvanceReadFlash(SIGNATURE_LENGTH, wFlashSize - SIGNATURE_LENGTH, pucBuffer)) {
		return FALSE;
	}
	if (pucBuffer[0] == 'C' && pucBuffer[1] == 'O' && pucBuffer[2] == 'N' && pucBuffer[3] == 'F'
	    && pucBuffer[4] == 'I' && pucBuffer[5] == 'G') {
		memcpy(&wSize, pucBuffer + 6, sizeof(WORD));
		if (wSize + (WORD) unFirmwareLength >= wFlashSize) {
			return FALSE;
		}
	}
	98 return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnDownloadConfig 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnDownloadConfig(UINT unConfigLength, VOID * pConfig)
{
	WORD wFlashSize;
	unConfigLength-- the file length of configuration file.pConfig-- the configuration binary file.
	    This function is used to download configuration file.if (!fnVerifyConfig(unConfigLength, pConfig)) {
		return FALSE;
	}
	if (!fnGetFlashSize(&wFlashSize)) {
		return FALSE;
	}
	if (!fnSetStartOffset(wFlashSize - (WORD) unConfigLength)) {
		return FALSE;
	}
	99 if (!fnAdvanceWriteFlash(unConfigLength, pConfig)) {
		return FALSE;
	}
	if (!fnCompareResult(unConfigLength, (wFlashSize - (WORD) unConfigLength), pConfig)) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnVerifyConfig 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnVerifyConfig(UINT unConfigLength, VOID * pConfig)
{
	WORD wFlashSize;
	WORD wSize;
	BYTE pucBuffer[SIGNATURE_LENGTH];
	//unConfigLength-- the file length of configuration file.pConfig-- the configuration binary file.
	//  This function is used to verify configuration file.
	// Check signature 
	if (*((BYTE *) pConfig + 0) != 'C' || *((BYTE *) pConfig + 1) != 'O' || *((BYTE *) pConfig + 2) != 'N'
	    || 100 * ((BYTE *) pConfig + 3) != 'F' || *((BYTE *) pConfig + 4) != 'I'
	    || *((BYTE *) pConfig + 5) != 'G') {
		return FALSE;
	}
	// Check overlap with config file 
	if (!fnGetFlashSize(&wFlashSize)) {
		return FALSE;
	}
	if (!fnAdvanceReadFlash(SIGNATURE_LENGTH, 0x0000, pucBuffer)) {
		return FALSE;
	}
	if (pucBuffer[0] == 'I' && pucBuffer[0] == 'T' && pucBuffer[0] == '7' && pucBuffer[0] == '2'
	    && pucBuffer[0] == '6' && pucBuffer[0] == '0' && pucBuffer[0] == 'F' && pucBuffer[0] == 'W') {
		memcpy(&wSize, pucBuffer + 12, sizeof(WORD));
		if (wSize + (WORD) unConfigLength >= wFlashSize) {
			return FALSE;
		}
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnEnterFirmwareUpgradeMode 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnEnterFirmwareUpgradeMode()
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	This function is used to enter firmware upgrade mode.
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x60;
	pucBuffer[1] = 0x00;
	pucBuffer[2] = 'I';
	pucBuffer[3] = 'T';
	pucBuffer[4] = '7';
	pucBuffer[5] = '2';

	//
	pucBuffer[6] = '6';

	//
	pucBuffer[7] = '0';

	//
	pucBuffer[8] = 0x55;

	//
	pucBuffer[9] = 0xAA;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 6, pucBuffer)) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnExitFirmwareUpgradeMode 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. This function is used to exit firmware upgrade mode. 103 

	// 
	// ***************************************************************************** 
BOOL fnExitFirmwareUpgradeMode()
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x60;
	pucBuffer[1] = 0x80;
	pucBuffer[2] = 'I';
	pucBuffer[3] = 'T';
	pucBuffer[4] = '7';
	pucBuffer[5] = '2';

	//
	pucBuffer[6] = '6';

	//
	pucBuffer[7] = '0';

	//
	pucBuffer[8] = 0xAA;

	//
	pucBuffer[9] = 0x55;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 6, pucBuffer)) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		104}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnGetFlashSize 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnGetFlashSize(WORD * pwFlashSize)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	//pwFlashSize-- the flash size of IT7260 / 50. This function is used to get IT7260 /
	//  50 flash size.
	if (pwFlashSize == NULL) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x01;
	pucBuffer[1] = 0x03;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 2, pucBuffer)) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 3, &wCommandResponse)) {
		return FALSE;
	}
	*pwFlashSize = wCommandResponse;
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnSetStartOffset 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnSetStartOffset(WORD wOffset)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	//wOffset-- the start offset of read / write flash.This function is used to set start offset of read /
	//write flash.
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x61;
	pucBuffer[1] = 0;
	memcpy(pucBuffer + 2, &wOffset, sizeof(WORD));
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 4, pucBuffer)) {
		return FALSE;
	}

	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnFirmwareReinitialize 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnFirmwareReinitialize()
{
	BYTE ucQuery;
	//This function is used to reset IT7260 / 50 firmware.108 
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	pucBuffer[0] = 0x6F;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 1, pucBuffer)) {
		return FALSE;
	}
	sleep(500);

	//sleep 0.5 second 
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	}
	while (ucQuery & QUERY_COMMAND_BUSY);
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnWriteFlash 
	// 
	// Description: 
	// 
	// This function is used to write flash data. 109 

	// Arguments: 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnWriteFlash(UINT unLength, VOID * pData)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	WORD wCommandResponse;
	BYTE ucLoop;
	//unLength-- the length of send flash data(Max length 128).pData-- the send flash data.
	if (unLength != 128 || pData == NULL) {
		return FALSE;
	}			/*Write 128 bytes into IC buffer by using command 0xF0 32 times */
	for (ucLoop = 0; ucLoop < unLength; ucLoop += 4) {
		do {
			if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
				ucQuery = QUERY_COMMAND_BUSY;
			}
		} while (ucQuery & QUERY_COMMAND_BUSY);
		pucBuffer[0] = 0xF0;
		pucBuffer[1] = ucLoop;
		memcpy(pucBuffer + 2, pData + ucLoop,
		       4 * sizeof(BYTE)) if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 6, pucBuffer)) {

			return FALSE;
		}
		do {
			if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
				ucQuery = QUERY_COMMAND_BUSY;
			}
		} while (ucQuery & QUERY_COMMAND_BUSY);
		if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
			return FALSE;
		}
		if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
			return FALSE;
		}
	}			/*Write 128 bytes into flash from IC buffer by using command 0xF1 */
	pucBuffer[0] = 0xF1;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 1, pucBuffer)) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}

	} while (ucQuery & QUERY_COMMAND_BUSY);
	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, 2, &wCommandResponse)) {
		return FALSE;
	}
	if (wCommandResponse != COMMAND_RESPONSE_SUCCESS) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnAdvanceWriteFlash 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnAdvanceWriteFlash(UINT unLength, VOID * pData)
{
	UINT unCurRoundLength;
	UINT unRemainderLength = unLength;
	//unLength-- the length of send flash data.pData-- the send flash data.This function is used to write flash data.
	while (unRemainderLength > 0) {
		if (unRemainderLength > 128) {
			112 unCurRoundLength = 128;
		} else {
			unCurRoundLength = unRemainderLength;
		}
		if (!fnWriteFlash(unCurRoundLength, pData + (unLength - unRemainderLength))) {
			return FALSE;
		}
		unRemainderLength -= unCurRoundLength;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnReadFlash 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnReadFlash(UINT unLength, UINT unOffset, VOID * pData)
{
	BYTE ucQuery;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	//unLength-- the length of receive flash data(Max length 4)..pData-- the receive flash data.
	//   This function is used to read flash data.113 
	if (unLength > 8 || pData == NULL) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);
	/*Read from flash: command: 0xE1, count, 0x01 mean byte, High Offset, Low Offset, 0x00, 0xD0 */
	pucBuffer[0] = 0xE1;
	pucBuffer[1] = (BYTE) unLength;
	pucBuffer[2] = 0x01;

	//byte 
	pucBuffer[3] = (BYTE) (unOffset & 0x00FF);
	pucBuffer[4] = (BYTE) ((unOffset & 0xFF00) >> 8);
	pucBuffer[5] = FLASH_LOW_ADDR;
	pucBuffer[6] = FLASH_HIGH_ADDR;
	if (!fnWriteBuffer(BUFFER_INDEX_COMMAND, 7, pucBuffer)) {
		return FALSE;
	}
	do {
		if (!fnReadBuffer(BUFFER_INDEX_QUERY, 1, &ucQuery)) {
			ucQuery = QUERY_COMMAND_BUSY;
		}
	} while (ucQuery & QUERY_COMMAND_BUSY);

	if (!fnReadBuffer(BUFFER_INDEX_COMMAND_RESPONSE, unLength, pData)) {
		return FALSE;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnAdvanceReadFlash 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnAdvanceReadFlash(UINT unLength, UINT unStartOffset, VOID * pData)
{
	UINT unCurRoundLength;
	UINT unRemainderLength = unLength;
	//unLength-- the length of receive flash data(Max length 128)..pData-- the receive flash data.
	//  This function is used to read flash data.
	while (unRemainderLength > 0) {
		if (unRemainderLength > 8) {
			unCurRoundLength = 8;
		} else {
			unCurRoundLength = unRemainderLength;
		115}
		if (!fnReadFlash(unCurRoundLength, unStartOffset, pData + (unLength - unRemainderLength))) {
			return FALSE;
		}
		unStartOffset += 8;
		unRemainderLength -= unCurRoundLength;
	}
	return TRUE;
}

	// ***************************************************************************** 
	// Function name: fnWriteBuffer 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// ucBuffeIndex -- the buffer index of IT7260/50 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnWriteBuffer(BYTE ucBuffeIndex, UINT unLength, VOID * pData)
{
	// Sudo code, not implement here 
}

	//unLength -- the file length of send data. pData -- the send data. This function is used to send data to IT7260/50. 
	// ***************************************************************************** 
	// Function name: fnReadBuffer 
	// 116 

	// Description: 
	// 
	// 
	// Arguments: 
	// ucBuffeIndex -- the buffer index of IT7260/50 
	// 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnReadBuffer(BYTE ucBuffeIndex, UINT unLength, VOID * pData)
{
	// Sudo code, not implement here 
}

	//unLength -- the file length of receive data. pData -- the receive data. This function is used to receive data from IT7260/50. 
	// ***************************************************************************** 
	// Function name: fnCompareResult 
	// 
	// Description: 
	// 
	// 
	// Arguments: 
	// unFileLength -- the file length 
	// 
	// 
	// Return Value: 
	// Return true if the function execute succefully, otherwise return false. 
	// 
	// ***************************************************************************** 
BOOL fnCompareResult(UINT unFileLength, UINT unStartOffset, VOID * pFile)
{
	UINT unCurRoundLength;
	UINT unRemainderLength = unFileLength;
	BYTE pucBuffer[MAX_BUFFER_SIZE];
	UINT unLoop;
	pFile-- the binary file.This function is used to compare the download file and data which reading from
	    flash.while (unRemainderLength > 0) {
		if (unRemainderLength > 8) {
			unCurRoundLength = 8;
		} else {
			unCurRoundLength = unRemainderLength;
		}
		if (!fnAdvanceReadFlash
		    (unCurRoundLength, unStartOffset + (unFileLength - unRemainderLength), pucBuffer)) {
			return FALSE;
		}
		for (unLoop = 0; unLoop < unCurRoundLength; unLoop++) {
			//compare 
			if (*(pFile + (unFileLength - unRemainderLength + unLoop)) != pucBuffer[unLoop]) {
				return FALSE;
			}
		}
		unRemainderLength -= unCurRoundLength;
	}
	return TRUE;
}
